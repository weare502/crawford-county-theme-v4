<?php
/**
 * The HomePage file
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}
$context = Timber::get_context();
$context['posts'] = Timber::get_posts( new WP_Query( array( 'posts_per_page' => 4 ) ) );
// $context['events'] = Timber::get_posts( tribe_get_events( array( 
// 	'posts_per_page' => 4,
// 	'tribeHideRecurrence' => 1,
// 	'custom_event_param' => true,
// 	'eventDisplay' => 'upcoming',
// 	'meta_query' => array(
// 		array(
// 			'key'     => 'featured',
// 			'value'   => true,
// 			'compare' => '=',
// 		),
// 	),
// ) ) );

// var_dump( $context['events']);
$context['slides'] = Timber::get_post( $post->ID );

$templates = array( 'home.twig' );

Timber::render( $templates, $context );
