<?php

$context = Timber::get_context();
$context['posts'] = Timber::get_posts();
date_default_timezone_set('America/Chicago');

if ( $_REQUEST['fullYear'] === '1' ){
	$this_year = new WP_Query(array(
		'post_type' => 'crawford_365', 
		'posts_per_page' => 365,
		'ignore_sticky_posts' => true,
		'year' => date('Y'),
		// 'monthnum' => date('n'), // no leading zeroes
		'order' => 'ASC'
		)
	);
	$context['this_year'] = Timber::get_posts( $this_year, "Crawford365Post" );
	foreach ( $context['this_year'] as &$slide ){
		$slide->post_content = strip_tags($slide->post_content, '<p><strong><bold><br>');
	}

	// var_dump($context['this_year']); die();
}

$slides = new WP_Query(array(
	'post_type' => 'crawford_365', 
	'posts_per_page' => 7,
	'ignore_sticky_posts' => true,
	'crawford_365_slider' => true,
	'date_query' => array(
		array(
			'before' => 'next sunday',
			'after' => 'last saturday'
		)
	),
	'order' => 'ASC'
	)
);

$this_month = new WP_Query(array(
	'post_type' => 'crawford_365', 
	'posts_per_page' => 31,
	'ignore_sticky_posts' => true,
	'year' => date('Y'),
	'monthnum' => date('n'), // no leading zeroes
	'order' => 'ASC'
	)
);

$context['hero'] = get_field('crawford_365_hero', 'options');
$context['social_img'] = get_field('crawford_365_social_image', 'options');
$context['social_link'] = get_field('crawford_365_social_link', 'options');
// $context['all'] = Timber::get_posts( array( 'post_type' => 'crawford_365', 'posts_per_page' => -1 ), "Crawford365Post" );
$context['this_month'] = Timber::get_posts( $this_month, "Crawford365Post" );
$context['slides'] = Timber::get_posts($slides->posts, "Crawford365Post");

// Strip Excel/Gsheets formatting BS span tags
foreach ( $context['slides'] as &$slide ){
	$slide->post_content = strip_tags($slide->post_content, '<p><strong><bold><br>');
}
foreach ( $context['this_month'] as &$slide ){
	$slide->post_content = strip_tags($slide->post_content, '<p><strong><bold><br>');
}

// Template expects an array. Grab the slide with the same date as today.
$context['today'] = array_filter( $context['slides'], function($item){
	return date('Y m j') === $item->date('Y m j');
});

// var_dump($context); die();
$context['crawford_365_cats'] = Timber::get_terms('crawford_365_category');

if ( isset( $context['this_year'] ) ) {
	Timber::render( "crawford-365-year.twig", $context );
} else {
	Timber::render( "crawford-365.twig", $context );
}