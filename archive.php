<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

$context = Timber::get_context();

$templates = array( 'archive.twig', 'index.twig' );

$context['title'] = 'Archive';
if ( is_day() ) {
	$context['title'] = 'Archive: '.get_the_date( 'D M Y' );
} else if ( is_month() ) {
	$context['title'] = 'Archive: '.get_the_date( 'M Y' );
} else if ( is_year() ) {
	$context['title'] = 'Archive: '.get_the_date( 'Y' );
} else if ( is_tag() ) {
	$context['title'] = single_tag_title( '', false );
} else if ( is_category() ) {
	$context['title'] = single_cat_title( '', false );
	array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
} else if ( is_post_type_archive() ) {
	$context['title'] = post_type_archive_title( '', false );
	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
} else {
	$context['title'] = get_post_type_object( $post->post_type )->labels->name;
}

$featured_query_args = array(
	// custom query var for pre_get_posts
	'archive_featured' => true,
	'post_type'  => $post->post_type,
	'posts_per_page' => 3,
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'meta_query' => array(
		array(
			'key'     => 'featured',
			'value'   => true,
			'compare' => '=',
		),
	),
);

$taxonomy = empty( $wp_query->query_vars['taxonomy'] ) ? '' : $wp_query->query_vars['taxonomy'];

if ( $taxonomy ){
	$term = get_term_by( 'slug', $wp_query->query_vars['term'], $taxonomy );
	$context['title'] = $term->name;
	$context['term_image'] = new TimberImage( get_term_meta( $term->term_id, 'image', true ) );
	$featured_query_args['tax_query'] = array(
		array(
			'taxonomy' => $taxonomy,
			'field' => 'slug',
			'terms' => $term->slug
		)
	);
}
global $wp_query;

if ( is_post_type_archive() && (count( $wp_query->query ) === 1) ) {
	$landing_page_id = new WP_Query( array( 
		'post_type' => 'landing_page',
		'meta_query' => array(
			array(
				'key'     => 'content_type',
				'value'   => $post->post_type . '_category',
				'compare' => '=',
			),
		),
		'posts_per_page' => 1
	) );

	$landing_page_id = $landing_page_id->posts[0]->ID;

	$context['before_terms'] = get_field('above_content', $landing_page_id);
	$context['after_terms'] = get_field('below_content', $landing_page_id);;
	$context['terms'] = Timber::get_terms( $post->post_type . '_category', array( 'hide_empty' => true ) );
	foreach ( $context['terms'] as $key => $term ){
		if ( $post->post_type === 'entertainment' ){
			if ( ! in_array( $term->ID, get_field( 'entertainment_selected_categories', 'options' ) ) ){
				unset( $context['terms'][$key] );
				continue;
			}
		}
		$thumb = new TimberImage( get_term_meta( $term->term_id, 'image', true ) );
		$term->thumbnail = $thumb;
		$term->get_preview = term_description( $term->ID, $post->post_type . '_category' );
		$context['terms'][$key] = $term;
	}
	// var_dump( $context['terms'][0] );

	$ad_terms = get_field( 'additional_boxes', $landing_page_id );
	if ( ! empty( $ad_terms ) ) {
		foreach ( $ad_terms as $t ){
			$term = new stdClass();
			$term->title = $t['title'];
			// $term->get_preview = $t['description'];
			$term->post_content = $t['description'];
			$term->link = $t['link'];
			if ( ! empty( $t['url_link'] ) ) {
				$term->link = $t['url_link'];
			}
			$term->thumbnail = new TimberImage( $t['image']['ID'] );
			$context['terms'][] = $term;
			// var_dump( $term->get_preview );
		}
	}
	// var_dump( $context['terms'] );
}

$context['featured'] = Timber::get_posts( new WP_Query( $featured_query_args ) );

$context['posts'] = Timber::get_posts();

Timber::render( $templates, $context );
