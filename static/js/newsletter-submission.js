/* global console */
(function($){
	$(document).ready(function(){
		var $form = $('#newsletter-signup-form');
		var $email = $('#newsletter-email');
		var $honeyPot = $('#hp-field');
		var $success = $('#newsletter-success');
		var $error = $('#newsletter-error');
		var $spinner = $('#newsletter-spinner');

		$form.on('submit', function( e ){
			e.preventDefault();

			// Reset our Error Messages & Spinner.
			$success.hide();
			$error.hide();
			$spinner.show();

			if ( '' !== $honeyPot.val().trim() ){
				console.log( 'You are a Bot. Goodbye!');
				$form.remove();
				return; // we have a bot!
			}

			var data = {
				input_values: {
					input_1: $email.val()
				}
			};

			$.ajax("/gravityformsapi/forms/1/submissions", {
				data: JSON.stringify( data ),
				type: 'POST',
				contentType: "application/json; charset=utf-8",
				dataType: "json",
				success: function( data ){
					console.log( data );
					if ( true === data.response.is_valid ){
						$success.show();
						$email.val('');
						$spinner.hide();
					} else {
						$error.show();
						$email.focus();
						$spinner.hide();
					}
				},
				failure: function(errMsg) {
					console.log(errMsg);
					$error.show();
					$spinner.hide();
				}
			});

		}); // form on.submit
	
	}); // docuemnt.ready
})(jQuery);