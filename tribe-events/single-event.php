<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

$events_label_singular = tribe_get_event_label_singular();
$events_label_plural = tribe_get_event_label_plural();

$event_id = get_the_ID();

add_filter( 'tribe_event_featured_image', function( $html ){
	return $html;
});

?>

<div id="tribe-events-content" class="tribe-events-single">

	<p class="tribe-events-back">
		<a href="<?php echo esc_url( tribe_get_events_link() ); ?>"> <?php printf( '&laquo; ' . esc_html__( 'All %s', 'the-events-calendar' ), $events_label_plural ); ?></a>
	</p>

	<!-- Notices -->
	<?php tribe_the_notices() ?>

	<div class="content-wrapper container">
		<article class="post-type-{{post.post_type}}" id="post-{{post.ID}}">
			<section class="article-content">
					<div class="article-header">
						<div class="main">
							<?php echo tribe_event_featured_image( $event_id, 'full', false ); ?>
						</div>
						<div class="header-meta side">
							<h1 class="article-h1"><?php the_title(); ?></h1>
						</div>

						

						<!-- Event header -->
						<div id="tribe-events-header" <?php tribe_events_the_header_attributes() ?>>
							<!-- Navigation -->
							<h3 class="tribe-events-visuallyhidden"><?php printf( esc_html__( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?></h3>
							<ul class="tribe-events-sub-nav">
								<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
								<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
							</ul>
							<!-- .tribe-events-sub-nav -->
						</div>
						<!-- #tribe-events-header -->

					</div>

			<?php while ( have_posts() ) :  the_post(); ?>
				<div class="article-bottom">
					<div class="article-body main">
						<!-- Event content -->
						<?php do_action( 'tribe_events_single_event_before_the_content' ) ?>
						<div class="tribe-events-single-event-description tribe-events-content">
							<?php the_content(); ?>
						</div>
						<!-- .tribe-events-single-event-description -->
						<?php do_action( 'tribe_events_single_event_after_the_content' ) ?>

					</div>

					<div class="meta-info side">
							<?php if ( tribe_get_cost() ) : ?>
									<span class="tribe-events-cost meta-item">Price: <?php echo tribe_get_cost( null, true ) ?></span>
							<?php endif; ?>

						<?php $meta_items = get_field( 'meta_links' );
							if ( ! empty( $meta_items ) ) :
							foreach ( $meta_items as $item ) : ?>
								<a class="meta-item" href="<?php echo $item['link']; ?>">
									<?php echo $item['label']; ?>
								</a>
							<?php endforeach; 

							endif; ?> 

						<!-- Event meta -->

					</div>

				</div>

				<div>
					<?php do_action( 'tribe_events_single_event_before_the_meta' ) ?>
						<?php tribe_get_template_part( 'modules/meta' ); ?>
						<?php do_action( 'tribe_events_single_event_after_the_meta' ) ?>
				</div>

		<?php if ( get_post_type() == Tribe__Events__Main::POSTTYPE && tribe_get_option( 'showComments', false ) ) comments_template() ?>
	<?php endwhile; ?>

	<!-- Event footer -->
	<div id="tribe-events-footer">
		<!-- Navigation -->
		<h3 class="tribe-events-visuallyhidden"><?php printf( esc_html__( '%s Navigation', 'the-events-calendar' ), $events_label_singular ); ?></h3>
		<ul class="tribe-events-sub-nav">
			<li class="tribe-events-nav-previous"><?php tribe_the_prev_event_link( '<span>&laquo;</span> %title%' ) ?></li>
			<li class="tribe-events-nav-next"><?php tribe_the_next_event_link( '%title% <span>&raquo;</span>' ) ?></li>
		</ul>
		<!-- .tribe-events-sub-nav -->
	</div>
	<!-- #tribe-events-footer -->

</div><!-- #tribe-events-content -->
