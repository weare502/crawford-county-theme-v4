<?php

if ( ! class_exists( 'Timber' ) ) {
	add_action( 'admin_notices', function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		} );
	return;
}

Timber::$dirname = array('templates', 'views');

class Crawford_County extends TimberSite {

	function __construct() {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'menus' );
		add_theme_support( 'title-tag');
		add_filter( 'timber_context', array( $this, 'add_to_context' ) );
		add_filter( 'get_twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		add_action( 'init', array( $this, 'register_acf_options_page' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ), 100 );
		add_action( 'pre_get_posts', array( $this, 'pre_get_posts' ), 100000 );
		add_action( 'tribe_events_pre_get_posts', array( $this, 'pre_get_posts' ), 10000 );

		add_action( 'admin_print_styles', function(){
			echo "<style type='text/css'>.toplevel_page_site-general-settings .acf-taxonomy-field .categorychecklist-holder { max-height: none; }</style>";
		}, 1000 );

		register_nav_menu( 'primary', 'Primary Header Menu' );
		register_nav_menu( 'secondary', 'Secondary Header Menu' );
		register_nav_menu( 'footer', 'Footer Menu' );
		parent::__construct();
	}

	function register_post_types() {
		//this is where you can register custom post types
	}

	function register_taxonomies() {
		//this is where you can register custom taxonomies
	}

	function register_acf_options_page(){
		if( function_exists('acf_add_options_page') ) {
			
			acf_add_options_page(array(
				'page_title' 	=> 'Site Settings',
				'menu_title'	=> 'Site Settings',
				'menu_slug' 	=> 'site-general-settings',
				'capability'	=> 'edit_posts',
				'redirect'		=> false,
				'position'		=> 40
			));
			
		}
	}

	function add_to_context( $context ) {
		$context['site'] = $this;
		$context['blog_url'] = $this->get_blog_url();

		// Menus & Navigation
		$context['menu'] = new TimberMenu( 'primary' );
		$context['secondary_menu'] = new TimberMenu( 'secondary' );
		$context['footer_menu'] = new TimberMenu( 'footer' );
		$context['breadcrumbs'] = function_exists('yoast_breadcrumb') ? yoast_breadcrumb( '<div id="breadcrumbs">', '</div>', false ) : "";

		// Post ID for "Front Page" or default to options when using ACF
		$context['home_id'] = get_option( 'page_on_front', 'options' );
		$context['SERVER'] = $_SERVER;

		// Footer Options
		$context['newsletter_heading'] = get_field( 'newsletter_heading', $context['home_id'] );
		$context['newsletter_description'] = get_field( 'newsletter_description', $context['home_id'] );
		$context['visitors_guide_heading'] = get_field( 'visitors_guide_heading', $context['home_id'] );
		$context['visitors_guide_description'] = get_field( 'visitors_guide_description', $context['home_id'] );
		
		return $context;
	}

	function myfoo( $text ) {
		$text .= ' bar!';
		return $text;
	}

	function get_blog_url(){
		if( $posts_page_id = get_option('page_for_posts', false ) ){
            return home_url( get_page_uri( $posts_page_id ) );
        } else {
            return home_url();
        }
	}

	function add_to_twig( $twig ) {
		/* this is where you can add your own fuctions to twig */
		$twig->addExtension( new Twig_Extension_StringLoader() );
		$twig->addFilter('myfoo', new Twig_SimpleFilter('myfoo', array($this, 'myfoo')));
		return $twig;
	}

	function pre_get_posts( $query ){
		global $post;
		if ( is_post_type_archive() && ! is_post_type_archive( array( 'tribe_events', 'post', 'page', 'attachment', 'crawford_365' ) ) && ! is_admin() && ! isset( $query->query_vars['archive_featured'] ) ){
			$query->set('orderby', 'title' );
			$query->set('order', 'ASC' );
			$query->set('posts_per_page', -1 );
		}

		if ( is_archive() && is_tax() && ! is_admin() ){
			$query->set('orderby', 'title' );
			$query->set('order', 'ASC' );
			$query->set('posts_per_page', -1 );
		}

		if ( isset( $query->query_vars['custom_event_param'] ) ){
			$query->set('tribeHideRecurrence', 1 );
			$query->set( 'posts_per_page', 4 );
			// var_dump( $query );
			// die();
		}

		if ( isset( $query->query_vars['archive_featured'] ) && isset($post->post_type) && $post->post_type === 'tribe_events' ){
			$query->set( 'posts_per_page', 4 );
		}

	}

	function enqueue_scripts(){
		wp_enqueue_script( 'bigSlide', "https://cdnjs.cloudflare.com/ajax/libs/bigslide.js/0.11.0/bigSlide.min.js", array('jquery'), '20160101', true );
		wp_enqueue_script( 'slick-carousel', "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.js", array('jquery'), '20160101', true );
		wp_enqueue_script( 'crawford-main', get_template_directory_uri() . "/static/js/min/site-min.js", array('jquery', 'bigSlide', 'slick-carousel', 'jquery-ui-accordion' ), '20160729', true );
		wp_enqueue_script( 'google-webfont', 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js', array(), '20160101', true );
		wp_add_inline_script( 'google-webfont', "WebFont.load({google: {families: ['Lato:300,400,700', 'Source Serif Pro:400,700']}});" );
		wp_enqueue_script( 'newsletter-submission', get_template_directory_uri() . "/static/js/newsletter-submission.js", array('jquery'), '20160101', true );
		wp_enqueue_style( 'slick-carousel', "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" );
	}

}

new Crawford_County();

/**
* Avoid a problem with Events Calendar PRO 4.2 which can inadvertently
* break oembeds.
*/
function crawford_undo_recurrence_oembed_logic() {
if ( ! class_exists( 'Tribe__Events__Pro__Main' ) ) return;
 
$pro_object = Tribe__Events__Pro__Main::instance();
$pro_callback = array( $pro_object, 'oembed_request_post_id_for_recurring_events' );
 
remove_filter( 'oembed_request_post_id', $pro_callback );
}
 
add_action( 'init', 'crawford_undo_recurrence_oembed_logic' );

add_action( 'tribe_events_after_template', function(){ ?>
	<div class="container centered submit-event">
		<a href="/events/community/add" class="button">Submit an Event</a>
	</div>
<?php });

add_action( 'tribe_events_before_template', function(){ ?>
	
	<div class="featured-title">Featured Events</div>

	<?php 

	add_filter( 'tribe_is_month', '__return_false' );
	add_filter( 'tribe_is_day', '__return_false' );

	$context['cards'] = Timber::get_posts( tribe_get_events( array( 
		'posts_per_page' => 4,
		'custom_event_param' => true,
		'tribeHideRecurrence' => 1,
		'eventDisplay' => 'list',
		'meta_query' => array(
			array(
				'key'     => 'featured',
				'value'   => true,
				'compare' => '=',
			),
		),
	) ) );

	remove_filter( 'tribe_is_month', '__return_false' );
	remove_filter( 'tribe_is_day', '__return_false' );

	$context['link_text'] = 'View Event';
	$context['bg'] = true;
	$context['truncate'] = 20;

	?>
	 <div class="container upcoming-events">
<?php
	Timber::render( 'event-card-style.twig', $context );
?>
	</div> <?php 

});

require('includes/class-crawford-365-post.php');

function crawford_limit_upload_size_limit_for_non_admin( $limit ) {
	$limit = '10000000'; // 10mb in bytes
	return $limit;
}
add_filter( 'upload_size_limit', 'crawford_limit_upload_size_limit_for_non_admin' );
 
function crawford_apply_wp_handle_upload_prefilter( $file ) {
    $limit = 10000000; // 1mb in bytes
    if ( $file['size'] > $limit ) {
    	$file['error'] = __( 'Maximum filesize is 10mb. Upload videos to YouTube or Vimeo.', 'crawford-county' );
    }
	return $file;
}

add_filter( 'wp_handle_upload_prefilter', 'crawford_apply_wp_handle_upload_prefilter' );

add_filter( 'wp_insert_post_data', 'crawford_do_not_set_posts_to_future' );
function crawford_do_not_set_posts_to_future( $data ) {
    if ( $data['post_status'] == 'future' && $data['post_type'] == 'crawford_365' )
        $data['post_status'] = 'publish';
    return $data;
}

