<?php

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

$landing_page_id = $post->ID;

$context['before_boxes'] = get_field('above_content', $landing_page_id);
$context['after_boxes'] = get_field('below_content', $landing_page_id);;
$context['boxes'] = [];

$ad_boxes = get_field( 'additional_boxes', $landing_page_id );
if ( ! empty( $ad_boxes ) ) {
	foreach ( $ad_boxes as $t ){
		$term = new stdClass();
		$term->title = $t['title'];
		// $term->get_preview = $t['description'];
		$term->post_content = $t['description'];
		$term->link = $t['link'];
		$term->thumbnail = new TimberImage( $t['image']['ID'] );
		$context['boxes'][] = $term;
		// var_dump( $term->get_preview );
	}
}

// var_dump($context['boxes']);
Timber::render( array( 'landing-page.twig' ), $context );