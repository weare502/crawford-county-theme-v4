<?php

class Crawford365Post extends TimberPost {
	public $day_365;

	public function get_day_365(){
		return date( 'z', strtotime($this->post_date) ) + 1;
	}
}